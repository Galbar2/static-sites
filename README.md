# Static Sites

Serve static websites backed by Markdown files!

## Try it!

We are going to build a server that serves the [demo
site](https://gitlab.com/Galbar2/static-sites/-/tree/master/demo_site).

First, install `static-sites`:
```bash
pip install static-sites static-sites-gitlab --extra-index-url https://gitlab.com/api/v4/projects/22149835/packages/pypi/simple
```

Then, write the [Applipy](https://gitlab.com/applipy/applipy/) configuration
file and save it as `dev.json`:
```json
{
  "app.name": "static-sites",
  "app.modules": [
    "static_sites.Module",
    "static_sites_gitlab.Module"
  ],
  "static_sites.sites": [
    {
      "host": "127.0.0.1:8080",
      "name": "Demo Site",
      "headers": {"Cache-Control": "public, max-age=3600;"},
      "storage.type": "gitlab",
      "storage.opts": {
        "project_id": 22149835,
        "path": "demo_site"
      }
    }
  ],
  "http.host": "127.0.0.1",
  "http.port": "8080",
  "logging.level": "INFO"
}
```

Finally, start Applipy:
```bash
python -m applipy
```

You can now access the site at [http://127.0.0.1:8080](http://127.0.0.1:8080).

## More

For more details check:
- [static-sites](static_sites/README.md), core module
- [static-sites-local](static_sites_local/README.md), storage implementation for local file system
- [static-sites-s3](static_sites_s3/README.md), storage implementation for S3
- [static-sites-gitlab](static_sites_gitlab/README.md), storage implementation for gitlab
