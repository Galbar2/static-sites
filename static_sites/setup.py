from setuptools import setup, find_packages
from distutils.util import convert_path
from os import path


ns = {}
ver_path = convert_path('static_sites/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), ns)
version = ns['__version__']


this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='static-sites',
    url='https://gitlab.com/Galbar2/static-sites',
    project_urls={
        'Source': 'https://gitlab.com/Galbar2/static-sites',
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Software Development :: Libraries',
    ],
    description='Applipy module to build services to serve static sites',
    long_description=long_description,
    long_description_content_type='text/markdown',
    license='Apache 2.0',
    author='Alessio Linares',
    author_email='mail@alessio.cc',
    version=version,
    packages=find_packages(),
    data_files=[],
    python_requires='>=3.6',
    install_requires=[
        'applipy>=1.1.0,<3.0.0',
        'applipy_http>=1.0.0,<3.0.0',
        'markdown>=3.3.3,<4.0.0',
    ],
    scripts=[],
)
