from applipy import Module as _Module, Config
from applipy_inject import with_names
from applipy_http import Api, Endpoint, HttpModule, PathFormatter, PrefixPathFormatter

from .endpoint import StaticEndpoint
from .site import SiteMapping


class Module(_Module):

    def __init__(self, config: Config):
        self._config = config

    def configure(self, bind, register):
        bind(SiteMapping)

        bind(Endpoint, StaticEndpoint, name='static_sites')
        base_path = self._config.get('static_sites.base_path')
        if base_path:
            bind(PathFormatter, PrefixPathFormatter(base_path), name='static_sites')
        else:
            bind(PathFormatter, name='static_sites')
        bind(with_names(Api, 'static_sites'), name=self._config.get('static_sites.server_name'))

    @classmethod
    def depends_on(cls):
        return HttpModule,
