from typing import List, Dict, Tuple, Any, Optional
from aiohttp import web
from asyncio import gather
from markdown import markdown
from applipy import Config
from applipy_http import Endpoint, Context
from .site import SiteMapping, Site
from .file_fetcher import FileFetcher, SiteFile
from .page import Page


_CONTENT_TYPE_FOR_EXTENSION = {
    'js': 'application/javascript',
    'json': 'application/json',
    'ogg': 'application/ogg',
    'pdf': 'application/pdf',
    'xml': 'application/xml',
    'zip': 'application/zip',
    'otc': 'font/collection',
    'ttc': 'font/collection',
    'otf': 'font/otf',
    'ttf': 'font/ttf',
    'woff': 'font/woff',
    'woff2': 'font/woff2',
    'bmp': 'image/bmp',
    'gif': 'image/gif',
    'jpeg': 'image/jpeg',
    'jpg': 'image/jpg',
    'png': 'image/png',
    'svg': 'image/svg+xml',
    'tiff': 'image/tiff',
    'ico': 'image/x-icon',
    'css': 'text/css',
    'csv': 'text/csv',
    'html': 'text/html',
    'txt': 'text/plain',
    'rtf': 'text/rtf',
    'mp4': 'video/mpeg',
    'webm': 'video/webm',
}

_HEADER_FILE = '_system/header.md'
_NAVIGATION_FILE = '_system/navigation.md'
_FOOTER_FILE = '_system/footer.md'
_STYLE_FILE = '_system/style.css'
_404_FILE = '_system/404.md'
_404_CSS_CLASS = '_404'

_SYSTEM_FILES = (_HEADER_FILE, _NAVIGATION_FILE, _FOOTER_FILE, _STYLE_FILE)


async def _read_file(filename: str, site_file: SiteFile) -> Tuple[str, str]:
    return filename, (await site_file.read()).decode('utf-8')


async def _get_files(file_fetcher: FileFetcher, site: Site, *files: str) -> Dict[str, str]:
    async with file_fetcher.fetch_files(site.storage, *files) as files:
        coros = (_read_file(name, file) for name, file in files.items() if not isinstance(file, FileNotFoundError))
        return dict(await gather(*coros))


def _is_blocked_path(site: Site, path: str) -> bool:
    return any(p.match(path) for p in site.blocked_paths)


def _is_blob_path(site: Site, path: str) -> bool:
    return any(p.match(path) for p in site.blob_paths)


async def _get_blob_response(request: web.Request,
                             fetcher: FileFetcher,
                             site: Site,
                             path: str,
                             tags: Optional[Dict[str, Any]]) -> web.StreamResponse:
    extension = path.split('.')[-1]
    async with fetcher.fetch_files(site.storage, path) as files:
        blob = files[path]
        if isinstance(blob, FileNotFoundError):
            return await _get_page_response(fetcher, site, _404_CSS_CLASS, _404_FILE, tags)

        if tags:
            tags['file'] = path

        web_response = web.StreamResponse()
        web_response.headers.update(site.headers)
        content_type = _CONTENT_TYPE_FOR_EXTENSION.get(extension.lower(), 'application/octet-stream')
        web_response.content_type = content_type
        await web_response.prepare(request)
        async for chunk in blob:
            await web_response.write(chunk)

        await web_response.write_eof()
        return web_response


async def _get_page_response(fetcher: FileFetcher,
                             site: Site,
                             css_class: str,
                             contents_file: str,
                             tags: Optional[Dict[str, Any]]) -> web.StreamResponse:

    files = await _get_files(fetcher, site, contents_file, *_SYSTEM_FILES)

    if contents_file not in files:
        _404_files = await _get_files(fetcher, site, _404_FILE)
        if _404_FILE not in _404_files:
            raise web.HTTPNotFound
        contents = _404_files[_404_FILE]
        css_class = _404_CSS_CLASS
        status_code = web.HTTPNotFound.status_code
        contents_file = _404_FILE
    else:
        contents = files[contents_file]
        status_code = web.HTTPNotFound.status_code if contents_file == _404_FILE else web.HTTPOk.status_code

    if tags:
        tags['file'] = contents_file

    title = site.name
    if contents_file != 'index.md' and contents.startswith('# '):
        subtitle = contents[2:contents.find('\n')]
        title = f'{subtitle} | {title}'

    page = Page(
        title,
        markdown(files[_HEADER_FILE]),
        markdown(files[_NAVIGATION_FILE]),
        markdown(contents),
        markdown(files[_FOOTER_FILE]),
        files[_STYLE_FILE],
        css_class,
    )
    return web.Response(
        status=status_code,
        text=page.render(),
        content_type='text/html',
        headers=site.headers,
    )


class StaticEndpoint(Endpoint):

    def __init__(self, sites: SiteMapping, file_fetchers: List[FileFetcher], config: Config) -> None:
        self.sites = sites
        self.file_fetchers = {f.type(): f for f in file_fetchers}
        self.track_files = config.get('static-sites.track_files', False)

    async def get(self, request: web.Request, context: Context) -> web.StreamResponse:
        metrics_tags = context.get('metrics.tags')
        if metrics_tags:
            metrics_tags['host'] = request.host

        path = request.match_info['path']

        try:
            site = self.sites[request.host]
        except KeyError:
            raise web.HTTPBadRequest()

        if metrics_tags:
            metrics_tags['storage_type'] = site.storage.type

        if _is_blocked_path(site, path):
            raise web.HTTPForbidden()

        file_fetcher = self.file_fetchers[site.storage.type]

        if not self.track_files:
            metrics_tags = None

        if _is_blob_path(site, path):
            return await _get_blob_response(request, file_fetcher, site, path, metrics_tags)

        path = path or 'index'
        contents_file = path + '.md'
        css_class = path.lstrip('/').replace('/', '_')
        return await _get_page_response(file_fetcher, site, css_class, contents_file, metrics_tags)

    async def options(self, request: web.Request, context: Context) -> web.StreamResponse:
        metrics_tags = context.get('metrics.tags')
        if metrics_tags:
            metrics_tags['host'] = request.host

        try:
            site = self.sites[request.host]
        except KeyError:
            raise web.HTTPBadRequest()

        return web.Response(status=200, headers=site.headers)

    def path(self) -> str:
        return r'/{path:.*}'
