class Page:

    _TEMPLATE = '''
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
{style}
</style>
<title>{title}</title>
</head>
<body class="{css_class}">
<header>
{header}
</header>
<nav>
{navigation}
</nav>
<article>
{contents}
</article>
<footer>
{footer}
</footer>
</body>
</html>
'''

    title: str
    header: str
    navigation: str
    contents: str
    footer: str
    style: str
    css_class: str

    def __init__(self,
                 title: str,
                 header: str,
                 navigation: str,
                 contents: str,
                 footer: str,
                 style: str,
                 css_class: str) -> None:
        self.title = title
        self.header = header
        self.navigation = navigation
        self.contents = contents
        self.footer = footer
        self.style = style
        self.css_class = css_class

    def render(self) -> str:
        return self._TEMPLATE.format(
            title=self.title,
            header=self.header,
            navigation=self.navigation,
            contents=self.contents,
            footer=self.footer,
            style=self.style,
            css_class=self.css_class,
        )
