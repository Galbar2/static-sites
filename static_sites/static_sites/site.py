import typing as T
import re
from applipy import Config


class StorageDef:

    type: str
    opts: T.Mapping

    def __init__(self, mapping: T.Mapping) -> None:
        self.type = mapping['type']
        self.opts = mapping['opts']


class Site:

    name: str
    storage: StorageDef
    blocked_paths: T.List[re.Pattern]
    blob_paths: T.List[re.Pattern]

    def __init__(self, mapping: T.Mapping) -> None:
        self.name = mapping['name']
        self.headers = mapping.get('headers', {})
        self.storage = StorageDef(mapping['storage'])
        self.blocked_paths = [
            re.compile(p) for p in mapping.get(
                'blocked_paths',
                [r'^_system/.*$']
            )
        ]
        self.blob_paths = [
            re.compile(p) for p in mapping.get(
                'blob_paths',
                [r'^([^\./]+(\.[^\./]+)+|([^\.][^\./]*/)+[^\./]+(\.[^\./]+)+)$']
            )
        ]


class SiteMapping(T.Dict[str, Site]):

    def __init__(self, config: Config) -> None:
        sites = config.get('static_sites.sites', [])
        for site_def in sites:
            site = Site(site_def)
            self[site_def['host']] = site

            for host in site_def.get('hosts', []):
                self[host] = site
