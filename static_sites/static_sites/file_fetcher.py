from typing import Dict, AsyncIterator, Union
from .site import StorageDef


class SiteFile:

    async def read(self, amt=None) -> bytes:
        raise NotImplementedError

    def __aiter__(self) -> AsyncIterator[bytes]:
        raise NotImplementedError

    async def __anext__(self) -> bytes:
        raise NotImplementedError


class FileFetchContext:

    async def __aenter__(self) -> Dict[str, Union[SiteFile, FileNotFoundError]]:
        raise NotImplementedError

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError


class FileFetcher:

    def fetch_files(self, storage_def: StorageDef, *filenames: str) -> FileFetchContext:
        raise NotImplementedError

    def type(self) -> str:
        raise NotImplementedError
