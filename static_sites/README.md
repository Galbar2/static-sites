# Static Sites

Applipy module to serve static websites from Markdown files.

This library implements the base logic but does not include an storage
implementation. For a storage you can use:
 - [S3 storage](../static_sites_s3/README.md)
 - [local storage](../static_sites_local/README.md)
 - [gitlab storage](../static_sites_gitlab/README.md)

The website is loaded from files in the defined storage.

## Site layout

The site HTML template and layout is as follows for all pages:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>
        { style }
        </style>

        <title>{ title }</title>
    </head>
    <body class="{ css_class }">
        <header>
        { header }
        </header>
        <nav>
        { navigation }
        </nav>
        <article>
        { contents }
        </article>
        <footer>
        { footer }
        </footer>
    </body>
</html>
```

## File structure

A tipical site file structure looks like this:
```
site
├── about.md
├── favicon.ico
├── blog
│   └── 2020-08-10-something.md
├── img
│   └── logo.png
├── index.md
└── _system
    ├── footer.md
    ├── header.md
    ├── navigation.md
    └── style.css
```

### \_system/

This directory contains the common content to be displayed in all site pages.

Each one of the files in it are used to fill the placeholders with the same
name in the HTML template above.

None of the files inside `_system` are publicly accessible. This can be changed
by overriding the [`blocked_paths`](#site).

### Markdown files

All markdown files will be served as pages of the site using their path
relative to the storage site's root directory, excluding their file extension.

For example:
 - `index.md` will be accessible at `/index`
 - `about.md` will be accessible at `/about`
 - `blog/2020-08-10-something.md` will be accessible at
   `/blog/2020-08-10-something`

### index.md

This file will be served both at `/` and `/index`. In other words, it is the
home page for the site.

### Media files

Any file is accessible raw by accessing its full path:
 - `/index.md` returns the raw Markdown file
 - `/favicon.ico` returns the raw `ico` file
 - `/img/logo.png` returns the raw `png` file

These files can be referenced from the Markdown files to display multimedia
content.

This can be changed by setting [`blob_paths`](#site)

## Inaccessible paths

By default:

- Any path starting with `_system/`

This can be changed by setting [`blocked_paths`](#site)

## CSS

Each page served will have its `<body>` tagged with a class equal to
`request.path.lstrip('/').replace('/', '_')`.

This enables custom styling for specific pages, for example the homepage

## 404 Not Found

By default the server will return a barebones `404: Not Found` page message if
no Markdown file is found for the page being accessed.

You can customize the look of this page by adding a Markdown file in
`_system/404.md`. This file will be loaded as the content of the page instead
of the file that could not be loaded, because it was not found.

The CSS class for the body in this case will be `_404`.

## Configuration Schema

Add to the configuration a section named `static_sites`:
```yaml
static_sites:
    sites: [<site>, ...]
    server_name: <string>
    base_path: <string>
    track_files: <bool>
```

Fields explanation
 - `sites`: (*Required*). List of sites to be served. This will be a list of [`site`s](#site).
 - `server_name`: (*Optional. Defaults to `None`*). Http server that will serve the static_sites endpoint.
 - `base_path`: (*Optional. Defaults to `''`*). Global base path for all sites served.
 - `track_files`: (*Optional. Defaults to `False`*). Whether the name of the file being served be included as a tag in the endpoint metrics..

### site

Description of the site to be served

```yaml
name: <string>
host: <string>
hosts: [<string>, ...]
headers: <headers>
storage: <storage>
blocked_paths: [<string>, ...]
blob_paths: [<string>, ...]
```

Fields explanation
 - `name`: (*Required*). Name of the site. It will be used for the title of the
   pages.
 - `host`: (*Required*). Main hostname of the site. I.e. `example.com`.
 - `hosts`: (*Optional. Defaults to `[]`*). List of alternate hostnames for the
   site. I.e. `[www.example.com, localhost]`.
 - `headers`: (*Optinal. Defaults to `{}`*). Headers to add to all responses.
   See [`headers` object](#headers).
 - `storage`: (*Required*). Storage configuration for the site. See [`storage`
   object](#storage).
 - `blocked_paths`: (*Optional. Defaults to `['^_system/.*$']`*). List of regex
   strings. All requests for paths that match any of these will return a `403:
   Forbidden`.
 - `blob_paths`: (*Optional. Defaults to
   `['^([^\./]+(\.[^\./]+)+|([^\.][^\./]*/)+[^\./]+(\.[^\./]+)+)$']`*). List of regex strings. All
   requests for paths that match any of these will fetch from storage the file
   as is and return it without any processing.

### headers

The headers object is a map of HTTP header name to value. It will be used for
all responses of the server.

Here is an example:
```yaml
Cache-Control: 'public, max-age=3600;'
Access-Control-Allow-Origin: '*'
```

### storage

The storage objects are a map that contain the configuration of the storage
backend from where to fetch the files.

A storage backend is an implementation of `static_sites.file_fetcher.FileFetcher`.
There are three implementations available in this project:
- [static-sites-local](../static_sites_local/README.md), storage implementation for local file system
- [static-sites-s3](../static_sites_s3/README.md), storage implementation for S3
- [static-sites-gitlab](../static_sites_gitlab/README.md), storage implementation for gitlab

```yaml
type: <string>
opts: <any>
```

Fields explanation
 - `type`: (*Required*). Type of the storage backend. It will be used to select
   the `FileFetcher` from the ones installed in the application.
 - `opts`: (*Required*). Any parameters that the storage backend requires.
   Check the storage documentation to see the specific options.

## Example

Applipy config:

```yaml
app:
  name: static-sites
  modules:
    - static_sites.Module
    - static_sites_local.Module
    - static_sites_s3.Module

static_sites.sites:
    # Host name that serves the site
  - host: blog.example.com
    # Title for the site
    name: My Cool Blog
    # Headers to add in the response
    headers:
      Cache-Control: 'public, max-age=3600;'
    # Storage configuration, used to load the site files
    storage:
      type: s3
      opts:
        bucket: xxx
        region_name: xxx
        aws_access_key_id: xxx
        aws_secret_access_key: xxx

    # Host name that serves the site
  - host: localhost
    # Title for the site
    name: Test Site
    # Storage configuration, used to load the site files
    storage:
      type: local
      opts:
        path: ./site/

http:
  host: 0.0.0.0
  port: 80
```
