import sys
from typing import Dict, AsyncIterator, List, Union
from asyncio import gather
from aiobotocore import AioSession
from aiobotocore.response import StreamingBody
from botocore.exceptions import ClientError
from static_sites.site import StorageDef
from static_sites.file_fetcher import SiteFile, FileFetcher, FileFetchContext


class S3SiteFile(SiteFile):

    def __init__(self, body: StreamingBody) -> None:
        self._body = body

    async def read(self, amt=None) -> bytes:
        return await self._body.read(amt)

    def __aiter__(self) -> AsyncIterator[bytes]:
        return self._body.__aiter__()

    async def __anext__(self) -> bytes:
        return await self._body.__anext__()


async def _get_file_contents(client, bucket: str, filename: str, prefix: str) -> str:
    try:
        key = prefix + filename
        response = await client.get_object(Bucket=bucket, Key=key)
        return filename, S3SiteFile(response['Body'])
    except ClientError:
        raise FileNotFoundError(key)


class S3FileFetchContext(FileFetchContext):

    def __init__(self, session: AioSession, storage_def: StorageDef, filenames: List[str]) -> None:
        self._session = session
        self._opts = storage_def.opts
        self._filenames = filenames
        self._client_creation_ctx = None

    async def __aenter__(self) -> Dict[str, Union[SiteFile, FileNotFoundError]]:
        self._client_creation_ctx = self._session.create_client(
            's3',
            region_name=self._opts.get('region_name'),
            endpoint_url=self._opts.get('endpoint_url'),
            aws_access_key_id=self._opts.get('aws_access_key_id'),
            aws_secret_access_key=self._opts.get('aws_secret_access_key')
        )
        client = await self._client_creation_ctx.__aenter__()
        coros = (_get_file_contents(client, self._opts['bucket'], filename, self._opts.get('prefix', ''))
                 for filename in self._filenames)

        try:
            results = dict(t for t in await gather(*(coros), return_exceptions=True) if not isinstance(t, Exception))
        except Exception:
            ex_type, ex, tb = sys.exc_info()
            await self.__aexit__(ex_type, ex, tb)
            raise

        for filename in self._filenames:
            if filename not in results:
                results[filename] = FileNotFoundError(f"Key `{filename}` not found in Buket `{self._opts['bucket']}`")

        return results

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if self._client_creation_ctx is not None:
            await self._client_creation_ctx.__aexit__(exc_type, exc_val, exc_tb)
        self._client_creation_ctx = None


class S3FileFetcher(FileFetcher):

    def __init__(self, boto_session: AioSession) -> None:
        self.session = boto_session

    def fetch_files(self, storage_def: StorageDef, *filenames: str) -> FileFetchContext:
        if storage_def.type != self.type():
            raise ValueError(storage_def.type)

        return S3FileFetchContext(self.session, storage_def, filenames)

    def type(self) -> str:
        return 's3'
