from applipy import Module as Module_
from aiobotocore import AioSession, get_session
from static_sites.file_fetcher import FileFetcher
from .s3_fetcher import S3FileFetcher


def boto_session_provider() -> AioSession:
    return get_session()


class Module(Module_):

    def configure(self, bind, configure):
        bind(boto_session_provider)
        bind(FileFetcher, S3FileFetcher)
