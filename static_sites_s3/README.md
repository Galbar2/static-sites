# Static Sites S3

Storage backend implementation for [static-sites](../static_sites/README.md) to
serve sites stored in a S3 compatible service.

## Specification

**type**: `s3`

**options**:
 - `bucket`: (*Required*). Name of the S3 bucket containing the files.
 - `region_name`: (*Optional. Defaults to `null`*). AWS region to connect to
 - `aws_access_key_id`: (*Optional. Defaults to `null`*). AWS access key id to
   read from the bucket.
 - `aws_secret_access_key`: (*Optional. Defaults to `null`*). AWS secret access
   key to read from the bucket.
 - `endpoint_url`: (*Optional. Defaults to `null`*). S3 endpoint URL, useful if
   connecting to non-AWS S3 compatible service.
 - `prefix`: (*Optional. Defaults to `''`*). Will be prepended to the file path
   when fetching it in the bucket.  Lets you keep the test and prod sites in
   the same bucket, with different prefixes.

## Example

Applipy config:

```yaml
static_sites:
  - host: blog.example.com
    name: My Cool Blog
    storage:
      type: s3
      opts:
        bucket: my-blog
        region_name: eu-west-1
        aws_access_key_id: xxx
        aws_secret_access_key: xxx
        endpoint_url: https://s3.aws.com
        prefix: prod/
```
