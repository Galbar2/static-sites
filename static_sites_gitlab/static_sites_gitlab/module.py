from applipy import AppHandle, Module as Module_
from static_sites.file_fetcher import FileFetcher
from .gitlab_fetcher import GitlabFileFetcher
from .client import GitlabClient


class GitlabClientHandle(AppHandle):

    def __init__(self, client: GitlabClient):
        self.client = client

    async def on_init(self):
        await self.client.open()

    async def on_shutdown(self):
        await self.client.close()


class Module(Module_):

    def configure(self, bind, register):
        register(GitlabClientHandle)

        bind(GitlabClient)
        bind(FileFetcher, GitlabFileFetcher)
