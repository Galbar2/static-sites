from aiohttp import StreamReader
from static_sites.file_fetcher import SiteFile


class GitlabSiteFile(SiteFile):

    _DEFAULT_CHUNK_SIZE = 1024

    def __init__(self, content: StreamReader) -> None:
        self._content = content

    async def read(self, amt=-1) -> bytes:
        return await self._content.read(amt)

    def __aiter__(self):
        return self._content.__aiter__()

    async def __anext__(self):
        return await self._content.__anext__()
