import sys
from os import path
from typing import Dict, List, Union, Optional, Tuple
from asyncio import gather
from static_sites.site import StorageDef
from static_sites.file_fetcher import SiteFile, FileFetcher, FileFetchContext

from .client import GitlabClient
from .file import GitlabSiteFile


async def _get_file_contents(client: GitlabClient,
                             base_url: str,
                             project_id: int,
                             access_token: Optional[str],
                             branch: str,
                             filename: str,
                             path_prefix: str) -> Tuple[str, Union[SiteFile, FileNotFoundError]]:
    try:
        full_path = path.join(path_prefix, filename)
        async with client.fetch(base_url, project_id, access_token, branch, full_path) as stream:
            yield filename, GitlabSiteFile(stream)
    except FileNotFoundError as e:
        yield filename, e
    except Exception as e:
        yield filename, FileNotFoundError(f'{e.__class__.__name__}({str(e)}')


class GitlabFileFetchContext(FileFetchContext):

    def __init__(self, client: GitlabClient, storage_def: StorageDef, filenames: List[str]) -> None:
        self._client = client
        self._opts = storage_def.opts
        self._filenames = filenames

    async def __aenter__(self) -> Dict[str, Union[SiteFile, FileNotFoundError]]:
        base_url = self._opts.get('base_url', 'https://gitlab.com')
        project_id = int(self._opts['project_id'])
        access_token = self._opts.get('access_token')
        branch = self._opts.get('branch', 'master')
        path_prefix = self._opts.get('path', '')

        async_iters = (
            _get_file_contents(self._client, base_url, project_id, access_token, branch, filename, path_prefix)
            for filename in self._filenames
        )
        coros = (it.__anext__() for it in async_iters)
        try:
            results = dict(t for t in await gather(*(coros), return_exceptions=True) if not isinstance(t, Exception))
        except Exception:
            ex_type, ex, tb = sys.exc_info()
            await self.__aexit__(ex_type, ex, tb)
            raise

        return results

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        ...


class GitlabFileFetcher(FileFetcher):

    def __init__(self, client: GitlabClient) -> None:
        self._client = client

    def fetch_files(self, storage_def: StorageDef, *filenames: str) -> FileFetchContext:
        if storage_def.type != self.type():
            raise ValueError(storage_def.type)

        return GitlabFileFetchContext(self._client, storage_def, filenames)

    def type(self) -> str:
        return 'gitlab'
