import sys
from typing import Optional
from asyncio import Future
from aiohttp import ClientSession, StreamReader, ClientResponseError
from contextlib import asynccontextmanager
from urllib.parse import quote


class GitlabClient:

    _URL_FORMAT = '{base}/api/v4/projects/{project_id}/repository/files/{filename}/raw?ref={branch}'

    def __init__(self) -> None:
        self._session = ClientSession()
        self._client_future = Future()

    async def open(self) -> None:
        if not self._client_future.done():
            client = await self._session.__aenter__()
            self._client_future.set_result(client)

    async def close(self) -> None:
        if self._client_future.done():
            ex_type, ex, tb = sys.exc_info()
            await self._session.__aexit__(ex_type, ex, tb)

    @asynccontextmanager
    async def fetch(self,
                    base_url: str,
                    project_id: int,
                    access_token: Optional[str],
                    branch: str,
                    filename: str) -> StreamReader:
        client: ClientSession = await self._get_client()
        url = self._URL_FORMAT.format(
            base=base_url.rstrip('/'),
            project_id=project_id,
            filename=quote(filename, safe=''),
            branch=branch,
        )
        headers = None
        if access_token:
            headers = {'PRIVATE-TOKEN': access_token}
        async with client.get(url, headers=headers) as resp:
            try:
                resp.raise_for_status()
            except ClientResponseError:
                raise FileNotFoundError(url)
            else:
                yield resp.content

    async def _get_client(self) -> ClientSession:
        return await self._client_future
