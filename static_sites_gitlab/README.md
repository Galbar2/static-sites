# Static Sites Gitlab

Storage backend implementation for [static-sites](../static_sites/README.md) to
serve sites stored as repos in gitlab.

## Specification

**type**: `gitlab`

**options**:
 - `base_url`: (*Optional. Defaults to `'https://gitlab.com'`*). Base url where
   the gitlab API is at.
 - `project_id`: (*Required*). ID of the gitlab project from where to fetch the
   files.
 - `access_token`: (*Optional. Defaults to `null`*). Gitlab API token. Useful
   to access private repositories.
 - `branch`: (*Optional. Defaults to `'master'`*). Branch from which to load
   the files.
 - `path`: (*Optional. Defaults to `''`*). Path, inside the repository, in
   which the site files are.

## Example

Applipy config:

```yaml
static_sites:
  - host: blog.example.com
    name: My Cool Blog
    storage:
      type: gitlab
      opts:
        base_url: api.my-gitlab-instance.com
        project_id: 123456789
        access_token: xxx
        branch: blog
        path: site
```
