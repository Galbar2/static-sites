# Static Sites Local

Storage backend implementation for [static-sites](../static_sites/README.md) to
serve sites stored in the local file system.

## Specification

**type**: `local`

**options**:
 - `path`: (*Optional. Defaults to `''`*). Relative or absolute path to
   directory containing the site files.

## Example

Applipy config:

```yaml
static_sites:
  - host: blog.example.com
    name: Test Site
    storage:
      type: local
      opts:
        path: ./site/
  - host: example.com
    name: Test Site
    storage:
      type: local
      opts:
        path: /var/site
```
