from applipy import Module as Module_
from static_sites.file_fetcher import FileFetcher
from .local_fetcher import LocalFileFetcher


class Module(Module_):

    def configure(self, bind, configure):
        bind(FileFetcher, LocalFileFetcher)
