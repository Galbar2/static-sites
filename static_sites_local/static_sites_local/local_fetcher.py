from typing import Dict, AsyncIterator, List, Union
from io import BufferedReader
from os import path
from static_sites.site import StorageDef
from static_sites.file_fetcher import SiteFile, FileFetchContext, FileFetcher


class LocalSiteFile(SiteFile):

    _DEFAULT_CHUNK_SIZE = 1024

    def __init__(self, file: BufferedReader) -> None:
        self._file = file

    async def read(self, amt=None) -> bytes:
        return self._file.read(amt)

    def __aiter__(self) -> AsyncIterator[bytes]:
        return self

    async def __anext__(self) -> bytes:
        current_chunk = self._file.read(self._DEFAULT_CHUNK_SIZE)
        if current_chunk:
            return current_chunk
        raise StopAsyncIteration


class LocalFileFetchContext(FileFetchContext):

    def __init__(self, storage_def: StorageDef, filenames: List[str]) -> None:
        self._opts = storage_def.opts
        self._filenames = filenames
        self._file_ctxs = None

    async def __aenter__(self) -> Dict[str, Union[SiteFile, FileNotFoundError]]:
        self._file_ctxs = {}
        for filename in self._filenames:
            full_path = path.join(self._opts.get('path', ''), filename)
            try:
                self._file_ctxs[filename] = open(full_path, 'rb')
            except FileNotFoundError as e:
                self._file_ctxs[filename] = e

        files = {}
        for filename, ctx in self._file_ctxs.items():
            if isinstance(ctx, FileNotFoundError):
                files[filename] = ctx
            else:
                files[filename] = LocalSiteFile(ctx.__enter__())

        return files

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if self._file_ctxs is None:
            return

        for ctx in self._file_ctxs.values():
            if not isinstance(ctx, FileNotFoundError):
                ctx.__exit__(exc_type, exc_val, exc_tb)


class LocalFileFetcher(FileFetcher):

    def fetch_files(self, storage_def: StorageDef, *filenames: str) -> FileFetchContext:
        if storage_def.type != self.type():
            raise ValueError(storage_def.type)

        return LocalFileFetchContext(storage_def, filenames)

    def type(self) -> str:
        return 'local'
